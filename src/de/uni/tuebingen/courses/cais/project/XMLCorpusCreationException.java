/*
 * Copyright (C) 2013 Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni.tuebingen.courses.cais.project;

/**
 * @author Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 */
public class XMLCorpusCreationException extends Exception {

    /**
     * Creates a new instance of
     * <code>XMLCorpusCreationException</code> without detail message.
     */
    public XMLCorpusCreationException() {
        super("Can't create a corpus with the parameters specified.");
    }

    /**
     * Constructs an instance of
     * <code>XMLCorpusCreationException</code> with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    public XMLCorpusCreationException(String msg) {
        super(msg);
    }
}
