/*
 * Copyright (C) 2013 Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni.tuebingen.courses.cais.project;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import org.javatuples.Triplet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 */


public class AnswersDataWriter {
    //Standart encoding
    public static final String ENCODING = "UTF-8";

    private Tokenizer tokenizer;

    /**
     * Constructor.
     *
     * @param modelFile file with a mode for the tokenizer.
     */
    public AnswersDataWriter(String modelFile) {
        try {
            // Initialize a tokenizer for german
            tokenizer = new TokenizerME(new TokenizerModel(new FileInputStream(modelFile)));
        } catch (IOException ex) {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Save data to file.
     *
     * @param file output file name.
     * @param answers collection of answers.
     * @param mode natural number from 1 to 3. 1 meaning include only answer data, 3 include all data.
     */
    public void saveDataToFile(String file, Collection<Triplet<String,String,String>> answers, int mode){
        try (PrintWriter out = new PrintWriter(new OutputStreamWriter (new FileOutputStream(file), ENCODING))) {
            int i = 1;
            for (Triplet t : answers) {
                if (mode > 2) out.write(i++ + " text: " + t.getValue1() + System.lineSeparator());
                if (mode > 1) out.write("Q: " + t.getValue0() + System.lineSeparator());
                out.write((mode > 1 ? "A: " : "") + t.getValue2() + System.lineSeparator());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AnswersDataWriter.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    /**
     * Saves the data in the Exmaralda xml format.
     *
     * @param file name of the output file.
     * @param answers collection of answers.
     */
    public void saveDataForExmaralda(String file, Collection<Triplet<String,String,String>> answers){
        // Main tags Exmaralda xml format.
        final String EXMARALDA_ROOT_TAG = "basic-transcription";
        final String EXMARALDA_HEAD_TAG = "head";
        final String EXMARALDA_BODY_TAG = "basic-body";
        final String EXMARALDA_TIMELINE_TAG = "common-timeline";
        final String EXMARALDA_TLI_TAG = "tli";
        final String EXMARALDA_TIER_TAG = "tier";
        final String EXMARALDA_EVENT_TAG = "event";
        final String EXMARALDA_HEAD_STRING = "<meta-information>\n" +
                "<project-name></project-name>\n" +
                "<ud-meta-information></ud-meta-information>\n" +
                "</meta-information>";
        
        Document document = null;
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = documentBuilder.newDocument();
            Element root = document.createElement(EXMARALDA_ROOT_TAG);
            // Create head element and a content substituter.
            Element head = document.createElement(EXMARALDA_HEAD_TAG);
            head.setTextContent("--HEAD_STRING--");
            root.appendChild(head);
            // Body element
            Element body = document.createElement(EXMARALDA_BODY_TAG);
            // Create a timeline for dummy timestamps
            Element timeline = document.createElement(EXMARALDA_TIMELINE_TAG);
            // Craete tiers and set the attributes
            Element tokensTier = document.createElement(EXMARALDA_TIER_TAG);
            tokensTier.setAttribute("id", "TIE0");
            tokensTier.setAttribute("category", "txt");
            tokensTier.setAttribute("display-name", "TXT");
            tokensTier.setAttribute("type", "t");
            Element questionsTier = document.createElement(EXMARALDA_TIER_TAG);
            questionsTier.setAttribute("id", "TIE1");
            questionsTier.setAttribute("category", "txt");
            questionsTier.setAttribute("display-name", "Q");
            questionsTier.setAttribute("type", "t");
            Element answeridTier = document.createElement(EXMARALDA_TIER_TAG);
            answeridTier.setAttribute("id", "TIE2");
            answeridTier.setAttribute("category", "txt");
            answeridTier.setAttribute("display-name", "ANSWER_ID");
            answeridTier.setAttribute("type", "t");
            Element textidTier = document.createElement(EXMARALDA_TIER_TAG);
            textidTier.setAttribute("id", "TIE3");
            textidTier.setAttribute("category", "txt");
            textidTier.setAttribute("display-name", "TEXT_ID");
            textidTier.setAttribute("type", "t");
            
            // Iterate over answers and their properties
            int id = 0;
            int answerId = 0;
            for (Triplet<String, String, String> triplet : answers) {
                // Tokenize the answer
                String[] tokens = tokenizer.tokenize(triplet.getValue2());
                int startId = id;
                // For every token ...
                for (String token : tokens) {
                    // ... craete a dummy timestamp ...
                    Element tli = document.createElement(EXMARALDA_TLI_TAG);
                    tli.setAttribute("id", "T" + id);
                    timeline.appendChild(tli);
                    // ... and put it into the tokens tier.
                    Element tokenEvent = document.createElement(EXMARALDA_EVENT_TAG);
                    tokenEvent.setAttribute("start", "T" + id);
                    tokenEvent.setAttribute("end", "T" + (id + 1));
                    tokenEvent.setTextContent(token);
                    tokensTier.appendChild(tokenEvent);
                    id++;
                }
                // For every answer add a question ...
                Element questionEvent = document.createElement(EXMARALDA_EVENT_TAG);
                questionEvent.setAttribute("start", "T" + startId);
                questionEvent.setAttribute("end", "T" + id);
                questionEvent.setTextContent(triplet.getValue0());
                questionsTier.appendChild(questionEvent);
                // ... and an id of a source text.
                Element textIdEvent = document.createElement(EXMARALDA_EVENT_TAG);
                textIdEvent.setAttribute("start", "T" + startId);
                textIdEvent.setAttribute("end", "T" + id);
                textIdEvent.setTextContent(triplet.getValue1());
                textidTier.appendChild(textIdEvent);
                Element answerIdEvent = document.createElement(EXMARALDA_EVENT_TAG);
                answerIdEvent.setAttribute("start", "T" + startId);
                answerIdEvent.setAttribute("end", "T" + id);
                answerIdEvent.setTextContent(String.valueOf(answerId));
                answeridTier.appendChild(answerIdEvent);
                answerId++;
            }
            // Add the timeline and tier to the document.
            body.appendChild(timeline);
            body.appendChild(tokensTier);
            body.appendChild(questionsTier);
            body.appendChild(answeridTier);
            body.appendChild(textidTier);
            root.appendChild(body);
            document.appendChild(root);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Write output to file.
        if (document != null){
            try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(file), ENCODING)) {
                out.write(XMLtoString(document).replace("--HEAD_STRING--", EXMARALDA_HEAD_STRING));
            } catch (IOException ex) {
                Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, "Error creating XML.");
        }
        
    }
    
    /**
     * Converts an XML Document to a String.
     *
     * @param document XML document
     * @return String XML document as a String
     */
    public static String XMLtoString(Document document) {
        String value = "";
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", 0);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            StringWriter stringWriter = new StringWriter();
            StreamResult result = new StreamResult(stringWriter);
            DOMSource source = new DOMSource(document);
            transformer.transform(source, result);
            value = stringWriter.toString();
        } catch (TransformerFactoryConfigurationError | IllegalArgumentException | TransformerException ex) {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return value;
    }
    
}
