package de.uni.tuebingen.courses.cais.project;

import java.io.File;
import java.io.FileFilter;

/*
 * Based on:
 * http://docs.oracle.com/javase/tutorial/uiswing/components/filechooser.html
 * http://www.java2s.com/Code/JavaAPI/javax.swing/JFileChoosersetFileFilterFileFilterfilter.htm
 */


/**
 * Filters files based on extension criteria.
 *
 * @author Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 */
public class ExtensionFilter implements FileFilter{
     String extensions[];
     boolean acceptDirs;
     
     /**
      * Constructor to filter out files of a particular extension.
      * 
      * @param extension 
      */
     public ExtensionFilter(String extension){
         this(new String[] { extension }, false);
     }
     
     /**
      * Constructor to filter out files of a particular extension.
      * 
      * @param extension 
      * @param acceptDirs include directories or not.
      */
     public ExtensionFilter(String extension, boolean acceptDirs){
         this(new String[] { extension }, acceptDirs);
     }
     
     /**
      * Constructor to filter out files of several extensions.
      * 
      * @param extensions 
      */
     public ExtensionFilter (String[] extensions){
         this(extensions, false);
     }
     
    /**
      * Constructor to filter out files of several extensions.
      * 
      * @param extensions 
      * @param acceptDirs include directories or not.
      */
     public ExtensionFilter (String[] extensions, boolean acceptDirs){
         this.extensions = (String[]) extensions.clone();
         for (String extension : extensions) {
             extension = extension.toLowerCase();
         }
         this.acceptDirs = acceptDirs;
     }
     
     /**
      * Returns the extension of a file.
      * 
      * @param f file
      * @return extension, null if there is no extension.
      */
     public static String getExtension(File f) {
         String ext = null;
         String s = f.getName();
         int i = s.lastIndexOf('.');
         
         if (i > 0 &&  i < s.length() - 1) {
             ext = s.substring(i+1).toLowerCase();
         }
         return ext;
     }
     
     
     /**
      * Filters out a file based on an exception. 
      * 
      * @param file
      * @return 
      */
     public boolean accept(File file) {
         if (file.isDirectory()) {
             return acceptDirs;
         } else {
             String path = file.getAbsolutePath().toLowerCase();
             for (int i = 0, n = extensions.length; i < n; i++) {
                 String extension = extensions[i];
                 if ((path.endsWith(extension) && (path.charAt(path.length() - extension.length() - 1)) == '.')) {
                     return true;
                 }
             }
         }
         return false;
     }
}
