/*
 * Copyright (C) 2013 Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni.tuebingen.courses.cais.project;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.javatuples.Triplet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 */
public class XMLCrawler {    
    public static final String MODEL_DE = "models/de/de-token.bin";
    
    // XML tags and attributes
    private static final String QUESTIONS_LAYER_NAME = "Questions";
    private static final String QUESTION_NAME = "Question";
    private static final String QUESTION_STRING_NAME = "questionString";
    private static final String TEXT_ID_ATTR = "text_id";
    private static final String ANSWER_NAME = "StudentAnswer";
    private static final String ANSWER_TEXT_NAME = "answerText";
       
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Default values */
        String input = "";
        String exbOutput = null, txtOutput = null;
        // Search parameters
        String questionType = "";
        boolean directory = false;
        boolean filterNumbers = false;
        int minAnswerLengthChars = -1;
        int minAnswerLengthTokens = 5;
        int separate = 0;
        
        /* Process commandline arugements */
        Options options = new Options();
        //Boolean options
        options.addOption("?","help", false, "Print this message.");
        options.addOption("d", "directory", false, "Input is a directory.  "
                + "In this case all compatible files in the input directory will be processed.");
        options.addOption("f","filter-numbers", false, "Filter out answers which consist only of digits.");
        //Argument options
        options.addOption("q","question-type", true, "Type of the question. If not set, all question are taken."
                + "Ex.: Wann");
        options.addOption("s","separate", true, "The amount of answers to store separately. "
                + "The tool tries to find at most different answers for the selection");
        options.addOption("exb", true, "Exmaralda output file name.");
        options.addOption("txt", true, "Txt output file name.");
        options.addOption("mc","min-characters-length", true, "Minimum answer length in characters.");
        options.addOption("mt","min-tokens-length", true, "Minimum answer length in tokens. Is set to 5 tokens by defualt.");
        //A formatter is used to print out a help message.
        HelpFormatter formatter = new HelpFormatter();
        String header = "java -jar creg-data-extractor.jar [OPTION]... [inputFile or corpusDir] \noptions:";
        try {
            CommandLineParser parser = new GnuParser();
            // Parse the command line arguments
            CommandLine line = parser.parse(options, args);
            // Generate a help message if "help" option is used 
            // or there is some parameter missing.
            if(line.hasOption("?")) {
                formatter.printHelp(" ", header, options, "");
                System.exit(0);
            } else if (line.getArgs().length != 1){
                Logger.getLogger(XMLCrawler.class.getName()).log(Level.WARNING, "You must specify input.");
                formatter.printHelp(" ", header, options, "");
                System.exit(0);
            } else {
                //Store parameters if everything is fine.
                directory = line.hasOption("d");
                filterNumbers = line.hasOption("f");
                input = line.getArgs()[0];
                if (line.hasOption("q"))
                    questionType = line.getOptionValue("q");
                if (line.hasOption("mc"))
                    minAnswerLengthChars = Integer.parseInt(line.getOptionValue("mc"));
                if (line.hasOption("mt"))
                    minAnswerLengthTokens = Integer.parseInt(line.getOptionValue("mt"));
                if (line.hasOption("s"))
                    separate = Integer.parseInt(line.getOptionValue("s"));
                if (line.hasOption("exb"))
                    exbOutput = line.getOptionValue("exb");
                if (line.hasOption("txt"))
                    txtOutput = line.getOptionValue("txt");
            }
        } catch( ParseException ex) {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, "Arguments parsing failed.  Reason: " + ex.getMessage());
            formatter.printHelp(" ", header, options, "");
            System.exit(0);
        }
        // Finally check the output paramters.
        if (txtOutput == null && exbOutput == null)
            System.out.println("Warning! You haven't set any output files. Use -exb or -txt.");
        
        System.out.println("Data from: " + input);
        HashSet<Triplet<String, String, String>> results = null;
        /* Data collection */
        if (directory){
            // If input is a directory -> construct a corpus.
            try {
                results = new XMLCorpus(input, questionType, minAnswerLengthChars, minAnswerLengthTokens, filterNumbers).getAnswers();
            } catch (XMLCorpusCreationException ex) {
                Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // If not, just analyze a single file.
            results = new XMLCrawler().extractUniqueAnswers(new File(input), questionType, minAnswerLengthChars, minAnswerLengthTokens, filterNumbers);
        }     
        // Save data to file
        if (results != null){
            System.out.println("Collected unique answers (all): " + results.size());
            // Initialize Data Writer
            AnswersDataWriter adw = new AnswersDataWriter(MODEL_DE);
            LinkedList<Triplet<String, String, String>> resultsList = new LinkedList<>(results);
            // Separate answers from the collected data...
            if (separate > 0){
                LinkedList<Triplet<String, String, String>> separatedResults = new LinkedList<>();
                // ... with at most different questions.
                HashSet<String> seenQuestions = new HashSet<>();
                int refresh = 0;
                while (separate > 0) {
                    Triplet<String, String, String> qa = resultsList.pop();
                    // If the questions is unseen -> add the answer to the selection.
                    if (!seenQuestions.contains(qa.getValue0())){
                        seenQuestions.add(qa.getValue0());
                        separatedResults.add(qa);
                        separate--;
                        // if not -> leave the answer for later.
                    } else {
                        resultsList.offer(qa);
                        refresh ++;
                    }
                    // If it can't find any new questions -> clean the memory.
                    if (refresh == separate){
                        seenQuestions = new HashSet<>();
                        refresh = 0;
                    }
                }
                // Write collected data to the file.
                if (txtOutput != null) adw.saveDataToFile(txtOutput.replace(".txt", "") + "_Selected.txt", separatedResults, 3);
                if (exbOutput != null) adw.saveDataForExmaralda(exbOutput.replace(".exb", "") + "_Selected.exb", separatedResults);
            }
            // Write collected data to the file.
            if (txtOutput != null) adw.saveDataToFile(txtOutput, resultsList, 3);
            if (exbOutput != null) adw.saveDataForExmaralda(exbOutput, resultsList);
            System.out.println("Finished writting.");
        }
    }

    /**
     * Collects unique question-answer pairs from an xml document.
     * 
     * @param file name of the source file.
     * @param questionType type of a question.
     * @param minAnswerLengthChars minimum length of an answer.
     * @return HashSet<Triplet> a set of triplets: the question, text id and the answer
     */
    public HashSet<Triplet<String, String, String>> extractUniqueAnswers (File file, String questionType, int minAnswerLengthChars, int minAnswerLengthTokens, boolean filterNumbers){
        // Counters
        int questionCounter = 0, 
            answerCounter = 0,
            longAnswerCounter = 0;
        System.out.println("File: " + file.getName());
        HashSet<Triplet<String, String, String>> collectedAnswers = new HashSet<>();
        Pattern questionPattern = Pattern.compile(".*\\b" + questionType.toLowerCase() + "\\b.*");
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            Element root = document.getDocumentElement();
            // Separate the layer with questions
            Element questionsLayer = (Element) root.getElementsByTagName(QUESTIONS_LAYER_NAME).item(0);
            // Get a list of question blocks
            NodeList questions = questionsLayer.getElementsByTagName(QUESTION_NAME);
            System.out.println("Questions: " + questions.getLength());
            // Iterate over questions
            for (int i = 0; i < questions.getLength(); i++){
                Element question = (Element) questions.item(i);
                // Get a question text
                String questionText = getQuestionString(question);
                // Get id of a text the question is about
                String textId = question.getAttribute(TEXT_ID_ATTR);
                // If the question matches the desired type
                if (questionPattern.matcher(questionText.toLowerCase()).matches()){
//                if (questionText.toLowerCase().contains(questionType.toLowerCase())){
                    questionCounter++;
                    // Get the list of answers
                    NodeList answers = question.getElementsByTagName(ANSWER_NAME);
                    // Iterate over answers
                    for (int j = 0; j < answers.getLength(); j++){
                        Element answer = (Element) answers.item(j);
                        // Get the answer text
                        String answerText = getAnswerString(answer);
                        // If there was a textual answer ...
                        if (answerText != null){
                            answerCounter++;
                            // .. and it is not just a number and of some preset length or longer ...
                            if (answerText.length() >= minAnswerLengthChars && answerText.split("(\\s+|(?=[.,]\\s))").length >= minAnswerLengthTokens
                                    && (!filterNumbers || answerText.matches(".*\\w.*"))) {
                                longAnswerCounter ++;
                                // ... save it to the collection.
                                collectedAnswers.add(new Triplet(questionText, textId, answerText));
                            }
                        } else {
                            // If there no string in the current answer element.
                            // System.out.println(answer.getAttribute("id"));
                        }
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(XMLCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Print out the statistics.
        System.out.println(questionType + " questions: " + questionCounter);
        System.out.println(questionType + " answers: " + answerCounter);
        System.out.println(questionType + " long answers: " + longAnswerCounter);
        System.out.println("Collected unique answers: " + collectedAnswers.size() + "\n");
        return collectedAnswers;
    }
    
    /**
     * Get the question string from the element.
     * 
     * @param question element containing the question.
     * @return the question string.
     */
    private String getQuestionString(Element question){
        Node questionsStringElement = question.getElementsByTagName(QUESTION_STRING_NAME).item(0);
        return questionsStringElement == null ? null : questionsStringElement.getTextContent();
    }
    
    /**
     * Get the answer string from the element.
     * 
     * @param answer element containing the answer.
     * @return the answer string.
     */
    private String getAnswerString(Element answer){
        Node answerStringElement = answer.getElementsByTagName(ANSWER_TEXT_NAME).item(0);
        return answerStringElement == null ? null : answerStringElement.getTextContent();
    }
}
