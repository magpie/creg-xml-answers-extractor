package de.uni.tuebingen.courses.cais.project;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.javatuples.Pair;
import org.javatuples.Triplet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 */
public class InterAnnotatorStatistics {
    
    public static void main(String[] args) throws Exception {
//        if (args.length < 2){
//            throw new Exception("Wrong number of parameters");
//        }
        // Document
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document documentFirst = documentBuilder.parse("output/Wann-q-and-a_Selected_annotated_combined.exb");
        Element basicBody = (Element) documentFirst.getDocumentElement().getElementsByTagName("basic-body").item(0);
        Element tokensTier = (Element) basicBody.getElementsByTagName("tier").item(0);
        Element answersTier = (Element) basicBody.getElementsByTagName("tier").item(2);
        NodeList tokenEvents = tokensTier.getElementsByTagName("event");
        NodeList sentences = answersTier.getElementsByTagName("event");
        Element answerTypeTier = (Element) basicBody.getElementsByTagName("tier").item(4);
        // First annotator
        Element synRelTier1st = (Element) basicBody.getElementsByTagName("tier").item(5);
        Element focusTier1st = (Element) basicBody.getElementsByTagName("tier").item(7);
        boolean[] focusValuesFirst = getFocusValues(focusTier1st, tokenEvents.getLength());
        String[] synRealizationValuesFirst = getSynRealizationValues(synRelTier1st, tokenEvents.getLength());
        // Second annotator
        Element synRelTier2nd = (Element) basicBody.getElementsByTagName("tier").item(8);
        Element focusTier2nd = (Element) basicBody.getElementsByTagName("tier").item(10);
        boolean[] focusValuesSecond = getFocusValues(focusTier2nd, tokenEvents.getLength());
        String[] synRealizationValuesSecond = getSynRealizationValues(synRelTier2nd, tokenEvents.getLength());
        //        for (Triplet<Integer, Integer, String> triplet : getSentenceSynRelSpans((Element) sentencesFirst.item(10), synRealizationValuesFirst)) {
        //            System.out.println(triplet);
        //        }
        
        HashMap<String, Integer> synRealStat1st = new HashMap<>();
        HashMap<String, Integer> synRealStat2nd = new HashMap<>();
        
        // Compare
        System.out.println("Sentences: " + sentences.getLength());
        System.out.println("Tokens: " + tokenEvents.getLength());
        double focusDifference = 0;
        double syntacticDifference = 0;
        double focusSpansDifference = 0;
        double syntacticSpansDifference = 0;
        int spansAmount = 0;
        for (int i = 0; i < 126; i++){
            List<Element> tokensElements = getSentenceTokens((Element) sentences.item(i), tokensTier);
            for (int j = 0; j < tokensElements.size(); j++){
                Element tokenFirst = tokensElements.get(j);
                int startId = Integer.parseInt(tokenFirst.getAttribute("start").replace("T", ""));
                if (focusValuesFirst[startId] != focusValuesSecond[startId]) focusDifference++;
                if (!synRealizationValuesFirst[startId].equalsIgnoreCase(synRealizationValuesSecond[startId])) syntacticDifference++;
            }
            List<Pair<Integer, Integer>> spansFirst = getSentenceFocusSpans((Element) sentences.item(i), focusValuesFirst);
            List<Pair<Integer, Integer>> spansSecond = getSentenceFocusSpans((Element) sentences.item(i), focusValuesSecond);
            spansAmount += spansFirst.size() > spansSecond.size() ? spansFirst.size() : spansSecond.size();

            if (spansFirst.size() != spansSecond.size()) focusSpansDifference++;
            for (int f = 0; f < spansFirst.size() && f < spansSecond.size(); f++){
                Pair<Integer, Integer> spanF = spansFirst.get(f);
                Pair<Integer, Integer> spanS = spansSecond.get(f);
                if (spanF.getValue0().intValue() != spanS.getValue0().intValue() ||
                        spanF.getValue1().intValue() != spanS.getValue1().intValue())
                    focusSpansDifference++;
            }
            List<Triplet<Integer, Integer, String>> synSpansFirst = getSentenceSynRelSpans((Element) sentences.item(i), synRealizationValuesFirst);
            List<Triplet<Integer, Integer, String>> synSpansSecond = getSentenceSynRelSpans((Element) sentences.item(i), synRealizationValuesSecond);
            if (synSpansFirst.size() != synSpansSecond.size()) syntacticSpansDifference++;
            for (int f = 0; f < synSpansFirst.size() && f < synSpansSecond.size(); f++){
                Triplet<Integer, Integer, String> spanF = synSpansFirst.get(f);
                Triplet<Integer, Integer, String> spanS = synSpansSecond.get(f);
                if (spanF.getValue0().intValue() != spanS.getValue0().intValue() ||
                        spanF.getValue1().intValue() != spanS.getValue1().intValue() ||
                        !spanF.getValue2().equalsIgnoreCase(spanS.getValue2()))
                    syntacticSpansDifference++;
            }
            for (Triplet<Integer, Integer, String> triplet : synSpansFirst) {
                if (!synRealStat1st.containsKey(triplet.getValue2())){
                    synRealStat1st.put(triplet.getValue2(), 1);
                } else {
                    synRealStat1st.put(triplet.getValue2(), synRealStat1st.get(triplet.getValue2()) + 1);
                }
            }
            for (Triplet<Integer, Integer, String> triplet : synSpansSecond) {
                if (!synRealStat2nd.containsKey(triplet.getValue2())){
                    synRealStat2nd.put(triplet.getValue2(), 1);
                } else {
                    synRealStat2nd.put(triplet.getValue2(), synRealStat2nd.get(triplet.getValue2()) + 1);
                }
            }
            
        }
        double focusPercentAgree = 1 - (focusDifference / tokenEvents.getLength());
        double focusSpansPercentAgree = 1 - (focusSpansDifference / spansAmount);
        double syntacticPercentAgree = 1 - (syntacticDifference / tokenEvents.getLength());
        double syntacticSpansPercentAgree = 1 - (syntacticSpansDifference / spansAmount);
        
        double ff = 0;
        for (int f = 0; f < focusValuesFirst.length; f++) {
            if (focusValuesFirst[f]) ff++;
        }
        double yesf = ff/focusValuesFirst.length;
        double fs = 0;
        for (int f = 0; f < 1580; f++) {
            if (focusValuesSecond[f]) fs++;
        }
        double yess = fs/1580;
        double agree = (yesf * yess) + ((1-yesf) * (1-yess));
        double kappaFocus = (focusPercentAgree - agree) / (1 - agree);
        
        double agreeSyn = 0.1666;
        double kappaSyn = (syntacticPercentAgree - agreeSyn) / (1 - agreeSyn);
        double kappaSynSpan = (syntacticSpansPercentAgree - agreeSyn) / (1 - agreeSyn);
        
        System.out.println("Agreement:");
        System.out.println("Agree F: " + agree);
        System.out.println("Agree Syn: " + agreeSyn);
        System.out.println("Focus:" + focusPercentAgree);
        System.out.println("Focus kappa:" + kappaFocus);
        System.out.println("Focus by sent:" + focusSpansPercentAgree);
        System.out.println("SR:" + syntacticPercentAgree);
        System.out.println("SR kappa:" + kappaSyn);
        System.out.println("SR by sent:" + syntacticSpansPercentAgree);
        System.out.println("Sr by sent kappa:" + kappaSynSpan);
        
        NodeList answerTypeEvents = answerTypeTier.getElementsByTagName("event");
        HashMap<String, Integer> answerTypeStat = new HashMap<>();
        for (int a = 0; a < answerTypeEvents.getLength(); a++){
            Element event = (Element) answerTypeEvents.item(a);
            String type = event.getTextContent();
            if (!answerTypeStat.containsKey(type)){
                answerTypeStat.put(type, 1);
            } else {
                answerTypeStat.put(type, answerTypeStat.get(type) + 1); 
            }
        }
        
        System.out.println("Statistics:");
        System.out.println("Focus 1st: " + ff);
        System.out.println("Focus 2ND: " + fs);
        System.out.println(answerTypeStat);
        System.out.println(synRealStat1st);
        System.out.println(synRealStat2nd);
    }
    
    /**
     * Composes a list of tokens events (Dom Elements) based on on a sentence event.
     * 
     * @param sentenceEvent event that spans across a sentence
     * @param tokensLayer layer that contains tokens annotation
     * @return list of DOM elements
     */
    private static List<Element> getSentenceTokens(Element sentenceEvent, Element tokensLayer){
        int startId = Integer.parseInt(sentenceEvent.getAttribute("start").replace("T", ""));
        int endId = Integer.parseInt(sentenceEvent.getAttribute("end").replace("T", ""));
        NodeList tokenEvents = tokensLayer.getElementsByTagName("event");
        LinkedList<Element> tokens = new LinkedList<>();
        for (int i = startId; i < endId; i++){
            tokens.add((Element) tokenEvents.item(i));
        }
        return tokens;
    }
    
    /**
     * 
     * @param sentenceEvent
     * @param focusValues
     * @return 
     */
    private static List<Pair<Integer, Integer>> getSentenceFocusSpans(Element sentenceEvent, boolean[] focusValues){
        int startId = Integer.parseInt(sentenceEvent.getAttribute("start").replace("T", ""));
        int endId = Integer.parseInt(sentenceEvent.getAttribute("end").replace("T", ""));
        LinkedList<Pair<Integer, Integer>> spans = new LinkedList<>();
        int fStart = startId;
        for (int i = startId; i < endId; i++){
            if (focusValues[i] == true && ((i < focusValues.length && focusValues[i+1] == false) || 
                    i == endId - 1)){
                spans.add(new Pair<>(fStart, i - fStart + 1));
            }
            if (focusValues[i] == false) fStart = i+1;
        }
        return spans;
    }
    
    /**
     * 
     * @param sentenceEvent
     * @param synrelValues
     * @return 
     */
    private static List<Triplet<Integer, Integer, String>> getSentenceSynRelSpans(Element sentenceEvent, String[] synrelValues){
        int startId = Integer.parseInt(sentenceEvent.getAttribute("start").replace("T", ""));
        int endId = Integer.parseInt(sentenceEvent.getAttribute("end").replace("T", ""));
        LinkedList<Triplet<Integer, Integer, String>> spans = new LinkedList<>();
        int fStart = startId;
        for (int i = startId; i < endId; i++){
            if (!synrelValues[i].isEmpty() && ((i < synrelValues.length && (synrelValues[i+1].isEmpty() || !synrelValues[i+1].equals(synrelValues[i]))) || 
                    i == endId - 1)){
                spans.add(new Triplet<>(fStart, i - fStart + 1, synrelValues[i]));
                fStart = i + 1;
            }
            if (synrelValues[i].isEmpty()) fStart = i+1;
        }
        return spans;
    }
    
    /**
     * Composes a list of boolean values for tokens. Each item in a the list
     * corresponds to a tokens with the same id.
     * 
     * @param focusLayer layer with focus spans
     * @param numberOfTokens total number of tokens in the document
     * @return list of boolean values
     */
    private static boolean[] getFocusValues (Element focusLayer, int numberOfTokens){
        boolean[] focusValues = new boolean[numberOfTokens];
        // Get all focus events and mark those indicies that fall into focus.
        NodeList focusEvents = focusLayer.getElementsByTagName("event");
        for (int i = 0; i < focusEvents.getLength(); i++){
            Element focusEvent = (Element) focusEvents.item(i);
            int startId = Integer.parseInt(focusEvent.getAttribute("start").replace("T", ""));
            int endId = Integer.parseInt(focusEvent.getAttribute("end").replace("T", ""));
            for (int j = startId; j < endId; j++){
                focusValues[j] = true;
            }
        }
        return focusValues;
    }
    
    /**
     * Composes a list of syntactic realization values for tokens. Each item in 
     * a the list corresponds to a tokens with the same id.
     * 
     * @param syntacticRealizationLayer layer with syntactic realization spans
     * @param numberOfTokens total number of tokens in the document
     * @return list of string values
     */
    private static String[] getSynRealizationValues (Element syntacticRealizationLayer, int numberOfTokens){
        String[] synRealizationValues = new String[numberOfTokens];
        for (int i = 0; i < synRealizationValues.length; i++) {
            synRealizationValues[i] = "";
        }
        // Get all focus events and mark those indicies that fall into focus.
        NodeList synRealizationEvents = syntacticRealizationLayer.getElementsByTagName("event");
        for (int i = 0; i < synRealizationEvents.getLength(); i++){
            Element focusEvent = (Element) synRealizationEvents.item(i);
            int startId = Integer.parseInt(focusEvent.getAttribute("start").replace("T", ""));
            int endId = Integer.parseInt(focusEvent.getAttribute("end").replace("T", ""));
            String syntacticRealization = focusEvent.getTextContent();
            for (int j = startId; j < endId; j++){
                synRealizationValues[j] = syntacticRealization;
            }
        }
        return synRealizationValues;
    }
}
