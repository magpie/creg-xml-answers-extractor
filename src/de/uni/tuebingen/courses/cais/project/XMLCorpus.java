/*
 * Copyright (C) 2013 Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.uni.tuebingen.courses.cais.project;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import org.javatuples.Triplet;

/**
 *
 * @author Daniil Sorokin <daniil.sorokin@uni-tuebingen.de>
 */
public class XMLCorpus {
    
    private HashSet<Triplet<String, String, String>> answers;
    private int filesStored;

    /**
     * Creates a new corpus from a directory.
     * 
     * @param directoryName directory name.
     */
    public XMLCorpus(String directoryName, String answerType, int answerLengthChars, int answerLengthTokens, boolean filterNumbers) throws XMLCorpusCreationException {
        File dir = new File(directoryName);
        // Check if it is a directory.
        if (!dir.isDirectory())
            throw new XMLCorpusCreationException("The parameter given is not a directory.");
        // Initialize instance variables
        answers = new HashSet<>();
        filesStored = 0;
        XMLCrawler crawler = new XMLCrawler();
        // Find all xml files in all subdirectories.
        LinkedList<File> fileList = new LinkedList<>();
        fileList.add(dir);
        while (fileList.size() > 0){
            File file = fileList.pop();
            if (file.isDirectory()){
                // For each directory, add all files to the list.
                fileList.addAll(Arrays.asList(file.listFiles(new ExtensionFilter("xml", true))));
            } else if (file.isFile()){
                // For each file, parse it and store all tagged words
                answers.addAll(crawler.extractUniqueAnswers(file, answerType, answerLengthChars, answerLengthTokens, filterNumbers));
                filesStored++;
            }
        }
    }
    
    /**
     * List of all tagged words in a corpus.
     * 
     * @return list of tagged words.
     */
    public HashSet<Triplet<String, String, String>> getAnswers() {
        return answers;
    }
}
